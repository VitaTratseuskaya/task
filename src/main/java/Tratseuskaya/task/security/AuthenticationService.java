package Tratseuskaya.task.security;

import Tratseuskaya.task.entity.UserEntity;
import Tratseuskaya.task.exception.AppEntityNotFoundException;
import Tratseuskaya.task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository userRepository;

    @Transactional
    public UserEntity getAuthenticationUser() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth == null) {
            return null;
        }
        return userRepository
                .findByLogin(auth.getName())
                .orElseThrow(() -> new AppEntityNotFoundException("Not found!"));
    }
}
