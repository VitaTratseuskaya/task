package Tratseuskaya.task.security;

import Tratseuskaya.task.entity.UserEntity;
import Tratseuskaya.task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserEntity user = userRepository
                .findByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException
                        ("JwtUserDetailService -> user was not found exception: " + login));

        return new JwtUserDetails(
                user.getLogin(),
                user.getPassword(),
                user.getBlocked());
              //  Arrays.asList(user.getRole()));
    }
}
