package Tratseuskaya.task.security;

import Tratseuskaya.task.entity.UserEntity;
import Tratseuskaya.task.exception.JwtAuthenticationException;
import Tratseuskaya.task.exception.NotFoundIdException;
import Tratseuskaya.task.repository.UserRepository;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JwtTokenProvider {

    @Value("${jwt.secretKey}")
    private String secret;

    @Value("${jwt.expirationTime}")
    private long timeOut;

    private final UserRepository userRepository;

    @PostConstruct
    protected void init() {
        secret = Base64.getEncoder().encodeToString(secret.getBytes());
    }

    public String createToken(final String login) {
        UserEntity user = userRepository
                .findByLogin(login)
                .orElseThrow(() -> new NotFoundIdException("User not found by email"));

        Claims claims = Jwts.claims();
       // claims.put("roles", Arrays.asList(user.getRole()));
        claims.put("email", user.getLogin());
        Date now = new Date(); // java.util.Date
        Date validity = new Date(new Date().getTime() + timeOut);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    public Authentication getAuthentication(final String token) {
        final Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
        final JwtPrincipal principal = new JwtPrincipal(
                claims.getBody().get("login", String.class));
             //   claims.getBody().get("roles", List.class));

        return new JwtAuthentication(principal);
    }

    public String resolveToken(final HttpServletRequest req) {
        final String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public boolean validateToken(final String token) {
        try {
            final Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtAuthenticationException("JWT token is expired or invalid");
        }
    }

   /* private List<String> getRoleNames(final List<Role> roles) {
        return roles.stream().map(role -> "ROLE_" + role.name()).collect(Collectors.toList());
    }*/

}
