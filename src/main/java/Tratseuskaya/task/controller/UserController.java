package Tratseuskaya.task.controller;

import Tratseuskaya.task.dto.UserCreateDto;
import Tratseuskaya.task.dto.UserFullDto;
import Tratseuskaya.task.dto.UserUpdateDto;
import Tratseuskaya.task.dto.UserUpdatePasswordDto;
import Tratseuskaya.task.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class UserController {


    private final UserService userService;

    @PostMapping("/users")
    public UserFullDto create(@RequestBody UserCreateDto dto) {
        return userService.create(dto);
    }

    @PutMapping("/users")
    public UserFullDto update(@RequestBody UserUpdateDto dto) {
        return userService.update(dto);
    }

    @GetMapping("/users")
    public Page<UserFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return userService.findAll(page, size);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable  Integer id){
        userService.delete(id);
    }

    @PutMapping("/users/password")
    public void changePassword(@RequestBody UserUpdatePasswordDto dto) {
        userService.changePassword(dto);
    }

    @DeleteMapping("/users/{from, to}/removeFromBd")
    public void removeFromDB(@RequestParam Integer from, @RequestParam Integer to){
        userService.removeFromDB(from, to);
    }


}
