package Tratseuskaya.task.exception;

public class UniqueValuesIsTakenException extends RuntimeException{

    public UniqueValuesIsTakenException(String message) {
        super(message);
    }
}
