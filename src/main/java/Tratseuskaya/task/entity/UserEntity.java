package Tratseuskaya.task.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "users")
@Where(clause = "deleted_at IS NULL")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @Column(name = "blocked")
    private Boolean blocked;
}
