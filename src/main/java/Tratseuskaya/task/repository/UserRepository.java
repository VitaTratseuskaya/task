package Tratseuskaya.task.repository;

import Tratseuskaya.task.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    Optional<UserEntity> findByLogin(String login);

    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM users WHERE user_id >= :userIdFrom AND user_id <= :userIdTo")
    void removeFromDB(@Param("userIdFrom") Integer userIdFrom, @Param("userIdTo") Integer userIdTo);
}