package Tratseuskaya.task.mapper;


import Tratseuskaya.task.dto.UserCreateDto;
import Tratseuskaya.task.dto.UserFullDto;
import Tratseuskaya.task.dto.UserUpdateDto;
import Tratseuskaya.task.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserEntity map(UserCreateDto dto);

    UserFullDto map(UserEntity entity);

    List<UserFullDto> map(List<UserEntity> entities);

    UserEntity map(UserUpdateDto dto);


}
