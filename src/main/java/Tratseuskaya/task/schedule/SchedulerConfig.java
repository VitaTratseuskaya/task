package Tratseuskaya.task.schedule;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Slf4j
@Configuration
@EnableScheduling
@EnableAsync
@ConditionalOnProperty(name = "scheduler.enabled", matchIfMissing = true)
public class SchedulerConfig {

    @Scheduled(fixedRate = 600000)
    @Async
    public void closeAppSchedule() {

        Calendar calendar = new GregorianCalendar(2022, 05, 21,13,13,0);
        Date date = calendar.getTime();

        Date dateNow = new Date();

        if (dateNow.after(date)) {
            System.out.println("EXIT!");
            System.exit(0);
        }
    }
}
