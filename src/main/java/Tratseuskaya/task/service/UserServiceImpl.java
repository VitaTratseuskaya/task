package Tratseuskaya.task.service;

import Tratseuskaya.task.dto.UserCreateDto;
import Tratseuskaya.task.dto.UserFullDto;
import Tratseuskaya.task.dto.UserUpdateDto;
import Tratseuskaya.task.dto.UserUpdatePasswordDto;
import Tratseuskaya.task.entity.UserEntity;
import Tratseuskaya.task.exception.AppEntityNotFoundException;
import Tratseuskaya.task.exception.UniqueValuesIsTakenException;
import Tratseuskaya.task.exception.WrongUserPasswordException;
import Tratseuskaya.task.mapper.UserMapper;
import Tratseuskaya.task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;


    @Override
    @Transactional
    public UserFullDto create(UserCreateDto dto) {
        UserEntity entityToSave = userMapper.map(dto);
        entityToSave.setFullName(dto.getFullName());
        entityToSave.setLogin(dto.getLogin());
        entityToSave.setPassword(passwordEncoder.encode(dto.getPassword() + dto.getLogin()));
        entityToSave.setBlocked(false);

        Optional<UserEntity> userWithSameLogin = userRepository.findByLogin(entityToSave.getLogin());
        if (userWithSameLogin.isPresent()) {
            throw new UniqueValuesIsTakenException("Email is taken!");
        }

        UserEntity savedEntity = userRepository.save(entityToSave);
        log.info("User was successfully created");
        return userMapper.map(savedEntity);
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto dto) {
        UserEntity userForUpdate = userRepository
                .findById(dto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId()));

        userForUpdate.setUserId(dto.getUserId());
        userForUpdate.setFullName(dto.getFullName());

        UserEntity updatedUser = userRepository.save(userForUpdate);

        log.info("User was successfully updated");
        return userMapper.map(updatedUser);
    }


    @Override
    @Transactional
    public Page<UserFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<UserEntity> foundUsers = userRepository.findAll(pageable);
        Page<UserFullDto> dtos = foundUsers.map(entity -> userMapper.map(entity));
        log.info("Found users -> " + dtos.getNumberOfElements());
        return dtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        UserEntity foundUser = userRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        foundUser.setDeletedAt(Instant.now());
        userRepository.save(foundUser);

        log.info("User was successfully deleted");
    }

    @Override
    @Transactional
    public void changePassword(UserUpdatePasswordDto dto) {
        UserEntity userToUpdate = userRepository
                .findById(dto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + dto.getUserId()));

        if (!passwordEncoder.matches((dto.getOldPassword() + userToUpdate.getLogin()), userToUpdate.getPassword() )) {
            throw new WrongUserPasswordException("Wrong password");
        }

        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword() + userToUpdate.getLogin()));
        userRepository.save(userToUpdate);
        log.info("User was successfully updated");
    }

    @Override
    @Transactional
    public void removeFromDB(Integer from, Integer to) {
        userRepository.removeFromDB(from, to);

        log.info("User was successfully deleted from database!");
    }
}
