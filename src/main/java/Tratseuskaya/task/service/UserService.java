package Tratseuskaya.task.service;

import Tratseuskaya.task.dto.UserCreateDto;
import Tratseuskaya.task.dto.UserFullDto;
import Tratseuskaya.task.dto.UserUpdateDto;
import Tratseuskaya.task.dto.UserUpdatePasswordDto;
import org.springframework.data.domain.Page;

public interface UserService {

    UserFullDto create(UserCreateDto dto);

    UserFullDto update(UserUpdateDto dto);

    Page<UserFullDto> findAll(int page, int size);

    void delete(Integer id);

    void changePassword(UserUpdatePasswordDto dto);

    void removeFromDB(Integer from, Integer to);
}
