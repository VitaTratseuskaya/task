package Tratseuskaya.task.dto;

import lombok.Data;

@Data
public class UserUpdatePasswordDto {

    private Integer userId;
    private String oldPassword;
    private String newPassword;

}
