package Tratseuskaya.task.dto;

import lombok.Data;

@Data
public class UserUpdateDto {

    private Integer userId;
    private String fullName;
}
