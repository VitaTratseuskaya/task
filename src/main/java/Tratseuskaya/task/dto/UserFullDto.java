package Tratseuskaya.task.dto;

import lombok.Data;

@Data
public class UserFullDto {

    private Integer userId;
    private String login;
    private String fullName;
}
