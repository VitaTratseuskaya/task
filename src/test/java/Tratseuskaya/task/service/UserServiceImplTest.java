package Tratseuskaya.task.service;

import Tratseuskaya.task.dto.UserCreateDto;
import Tratseuskaya.task.dto.UserFullDto;
import Tratseuskaya.task.dto.UserUpdateDto;
import Tratseuskaya.task.entity.UserEntity;
import Tratseuskaya.task.mapper.UserMapper;
import Tratseuskaya.task.repository.UserRepository;
import Tratseuskaya.task.util.DataBaseCleaner;
import Tratseuskaya.task.util.GenerationUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;


@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DataBaseCleaner cleaner;

  /*  @BeforeEach
    public void setUp() {
        cleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        cleaner.clean();
    }*/


    @Test
    @Transactional
    public void create_happyPath() {
        //given
        UserCreateDto userToSave = GenerationUtil.prepareUserCreateDto();
        //when
        UserFullDto createdUser = userService.create(userToSave);
        //then
        Assertions.assertNotNull(createdUser.getUserId());
    }

    @Test
    @Transactional
    public void update_happyPath() {
        //given
        UserEntity user = addUserToDataBase();

        UserUpdateDto userUpdateDto = new UserUpdateDto();
        userUpdateDto.setUserId(user.getUserId());
        userUpdateDto.setFullName("test");

        //when
        UserFullDto updatedUser = userService.update(userUpdateDto);

        //then
        Assertions.assertEquals(user.getUserId(), updatedUser.getUserId());

        UserEntity afterUpdate = userRepository.getById(updatedUser.getUserId());
        Assertions.assertEquals(afterUpdate.getFullName(), updatedUser.getFullName());
    }

    @Test
    @Transactional
    public void findAll_happyPath() {
        //given

        //when
        Page<UserFullDto> allUsers = userService.findAll(0,  1);
        //then
        Assertions.assertEquals(1, allUsers.getNumberOfElements());
    }

    @Test
    @Transactional
    public void delete_happyPath() {
        //given
        UserEntity user = GenerationUtil.prepareUser();
        userRepository.save(user);
        //when
        userService.delete(user.getUserId());
        //then
        Assertions.assertNotNull(user.getDeletedAt());
    }

    private UserEntity addUserToDataBase() {
        UserEntity userToAdd = GenerationUtil.prepareUser();
        return userRepository.save(userToAdd);
    }
    private UserFullDto addUserDtoToDataBase() {
        UserCreateDto createDto = GenerationUtil.prepareUserCreateDto();
        return userService.create(createDto);

    }
}
