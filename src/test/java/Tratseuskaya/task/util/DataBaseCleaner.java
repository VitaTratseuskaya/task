package Tratseuskaya.task.util;

import Tratseuskaya.task.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataBaseCleaner {

    @Autowired
    private UserRepository userRepository;

    public void clean() {
        userRepository.deleteAllInBatch();
    }
}
