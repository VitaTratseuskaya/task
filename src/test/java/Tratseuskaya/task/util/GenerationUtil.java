package Tratseuskaya.task.util;

import Tratseuskaya.task.dto.UserCreateDto;
import Tratseuskaya.task.entity.UserEntity;
import Tratseuskaya.task.repository.UserRepository;


import static Tratseuskaya.task.TaskApplicationTests.FAKER;

public class GenerationUtil {

    public static UserRepository userRepository;

    public static UserEntity prepareUser() {
        UserEntity user = new UserEntity();

        user.setFullName(FAKER.name().firstName());
        user.setLogin(FAKER.internet().emailAddress());
        user.setPassword(FAKER.internet().password());
        user.setBlocked(false);

        return user;
    }
    public static UserCreateDto prepareUserCreateDto() {
        UserCreateDto user = new UserCreateDto();

        user.setFullName(FAKER.name().firstName());
        user.setLogin(FAKER.internet().emailAddress());
        user.setPassword(FAKER.internet().password());

        return user;
    }

}
